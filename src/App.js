import React from 'react';
import './App.css';
import 'react-flags-select/css/react-flags-select.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
/* Import redux */
import { createStore } from 'redux';
import { Provider } from 'react-redux';
/* Import page views */
import AuthorizationView from './components/AuthorizationView';
import HomeView from './components/HomeView';
import ContentView from './components/ContentView';

const reducerBig = function(){

}

const store = createStore(reducerBig);

const ContentId = ({match}) => {
  return(
      <ContentView 
        contentId={match.params.contentId}
      />
  );
}

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
            <Route path="/home" component={HomeView} />
            <Route path="/content/:contentId" component={ContentId} />
            <Route path="/" component={AuthorizationView} />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
