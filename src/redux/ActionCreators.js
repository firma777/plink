import * as ActionTypes from './ActionTypes';

export const slugRequest = (status) => ({
   type: ActionTypes.SLUG_REQUESTED,
   payload: status
});

export const slugReceived = (slug) => ({
   type: ActionTypes.SLUG_RECEIVED,
   payload: slug
});

export const qrGenerated = (status) => ({
   type: ActionTypes.QR_GENERATED,
   payload: status
});

export const qrExpired = (status) => ({
   type: ActionTypes.QR_EXPIRED,
   payload: status
});

export const tokenReceived = (token) => ({
   type: ActionTypes.TOKEN_RECEIVED,
   payload: token
});

export const tokenExpired = (status) => ({
   type: ActionTypes.TOKEN_EXPIRED,
   payload: status
});

export const languageSelected = (landuage) => ({
   type: ActionTypes.LANGUAGE_SELECTED,
   payload: landuage
});