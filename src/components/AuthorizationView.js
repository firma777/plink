import React from 'react';
import ReactFlagsSelect from 'react-flags-select';
import { Link } from "react-router-dom";
import WindowHeader from './WindowHeader';
import QRCode from 'qrcode';

class Authorization extends React.Component{
   constructor(props){
      super(props);
      this.props = props;
      this.qrCanvas = React.createRef();
      this.secSpan = React.createRef();
      this.TTL = 60;
      this.sec = this.TTL;
   }

   componentWillUpdate(){
      console.log(this.props.parentState);
   }

   timer(){
      if (this.sec < 1){
         this.sec = this.TTL;
         this.componentDidMount();
      }
      this.sec -= 1;
      this.secSpan.current.innerText = this.sec;
      console.log("ms: ", this.sec);
   }

   uodateQR(){
      if (this.intervalId){

      } else {
         this.intervalId = setInterval(this.timer.bind(this), 1000);
      }
   }

   componentWillUnmount(){
      clearInterval(this.intervalId);
   }

   componentDidMount(){
      let thisAuth = this;
      if(this.props.userToken){
         clearInterval(this.intervalId);
      } else {
         this.uodateQR();
         let updateParentState =  this.props.handleParentState;
         fetch("https://plink.tech/qrcode_authentications/", {method: 'POST'})
         .then(response => {
            if(response.ok){
               return response;
            } else {
               var error = new Error('Error ' + response.status + ': ' + response.statusText);
               error.response = response;
               throw error;
            }
         },
            error => {
               var errormess = new Error(error.message);
               throw errormess;
            }
         )
         .then(response => response.json())
         .then(response => {
            QRCode.toCanvas(this.qrCanvas.current, response.slug, { margin:0 }, function (error) {
               error ? console.error(error) : console.log('QR Code is generated successfully! ', response.slug);
            });

            let socket = new WebSocket(`wss://plink.tech/wss/qr_code_authentication/${response.slug}/`);
            socket.onopen = function() {
            // console.log("WEBSOCKET CONNECTION IS OPENED");
            updateParentState({isWSSConnection: true});
            };

            socket.onclose = function(event) {
               if (event.wasClean) {
               //   console.log('WEBSOCKET CONNECTION IS CLOSED PROPERLY');
               } else {
               //   console.log('WEBSOCKET CONNECTION IS LOST');
               }
               // console.log('CODE: ' + event.code + ' REASON: ' + event.reason);
               updateParentState({isWSSConnection: false});
            };

            socket.onmessage = function(event) {
               let receivedsonJData = JSON.parse(event.data);
               if (receivedsonJData.user_token){
                  updateParentState({ userToken: receivedsonJData.user_token, userTokenDate: new Date()});
                  clearInterval(thisAuth.intervalId);
               } else {
                  console.log("Received data: " + event.data);
               }
            };
         })
         .catch(error => console.log(error));
      }
   }

   render(){
      if(this.props.userToken){
         return(
               <div className="table-class">
                  <h2 className="h2-authorization-success">Ваш ПК успешно авторизирован!</h2>
               </div>
         );
      } else {
         return(
            <React.Fragment>
               <h2 className="h2-authorization">Отсканируйте QR-код через мобильное приложение</h2>
               <canvas id="qr-canvas" ref={this.qrCanvas}></canvas>
               <h2 className="qr-expire">QR-код будет действителен <span ref={this.secSpan}></span> сек.</h2>
            </React.Fragment>
         );
      }
   }
   
}

class AuthorizationView extends React.Component{
   constructor(props){
      super(props);
      this.state = {
         isWSSConnection: false,
         userToken: "",
         userTokenDate: "",
         activeLanguage: "RU"
      };
      this.onSelectFlag = this.onSelectFlag.bind(this);
      this.handleParentState = this.handleParentState.bind(this);
   }

   onSelectFlag(countryCode){
      this.setState({...this.state, activeLanguage: countryCode});
   }

   handleParentState(newState){
      this.setState({
         ...this.state, ...newState
       })
   }

   render(){
      return(
         <React.Fragment>
            <WindowHeader />
            <ReactFlagsSelect
               className="plink-flag-selector"
               defaultCountry="RU"
               countries={["US", "RU", "UA", "FR","DE","IT", "PL", "JP"]}
               showSelectedLabel={false}
               showOptionLabel={true}
               alignOptions="left"
               onSelect={this.onSelectFlag}
            />
            <div className="main-logo-authorization"></div>
            <h1 className="h1-authorization">Приветствуем, Plinker!</h1>
            <Authorization handleParentState={this.handleParentState} userToken={this.state.userToken} parentState={this.state}/>
            <div>
               <Link className="App-link" to="/content/1">Как присоеденить новое устройств?</Link>
               <h3 className="h3-authorization">Не можете отсканировать код?</h3>
               <Link className="App-link" to="/content/2">Попробуйте другой способ</Link>
            </div>
         </React.Fragment>
      );
   };
}


export default AuthorizationView;

