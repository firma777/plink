import React from 'react';
import { Link } from "react-router-dom";
import WindowHeader from './WindowHeader';

class ContentView extends React.Component{
   constructor(props){
      super(props);
      this.state = {
         isAuthorized: false,
         isWSSConnection: false,
         isUserToken: false
      }
   }

   render(){
      // some render functions will be defined here
      return(
         <React.Fragment>
            <WindowHeader />
            <h1>Content Page with id: {this.props.contentId}</h1>
            <Link className="App-link" to="/">-- Go home --</Link>
            <div className="flex-conteiner">
               <div className="block1">Block 1 text goes here...</div>
               <div className="block2">Lorem ipsum dolor...</div>
               <div className="block3">Block 3 text goes here...</div>
               <div className="block4">Some long link goes here...</div>
               <div className="block5">Some long link #32 goes here...</div>
            </div>
         </React.Fragment>
      );
   };
}


export default ContentView;

