import React from 'react';
const { remote } = window.require('electron');

class WindowHeader extends React.Component{
   constructor(options){
      super(options);
      this.btnClose = React.createRef();
      this.btnMinimize = React.createRef();
   }

   componentDidMount(){
      this.btnClose.current.addEventListener("click", e => {
         remote.getCurrentWindow().close();
      });

      this.btnMinimize.current.addEventListener("click", e => {
         remote.getCurrentWindow().minimize();
      });
   }

   render(){
      return(
         <div className="win-header">
            <div className="logo-mini"></div>
            <div className="btn-minimize" ref={this.btnMinimize}></div>
            <div className="btn-close" ref={this.btnClose}></div>
         </div>
      );
   }
}

export default WindowHeader;