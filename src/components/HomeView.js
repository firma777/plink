import React from 'react';
import WindowHeader from './WindowHeader';

class HomeView extends React.Component{
   constructor(props){
      super(props);
      this.state = {
         isAuthorized: false,
         isWSSConnection: false,
         isUserToken: false
      }
   }

   render(){
      // some render functions will be defined here
      return(
         <React.Fragment>
            <WindowHeader />
            <h1>Home Page</h1>
         </React.Fragment>
      );
   };
}


export default HomeView;

